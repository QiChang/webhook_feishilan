FROM docker.io/liyehaha/py3-venv:latest

RUN rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
RUN rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm

ENV PROJECT=prod
ENV LOG_PATH=/home/works/program/logs
ENV LANG en_US.UTF-8

ADD ./supervisord.conf /etc/supervisord.conf

RUN mkdir -p /home/works/program/

ADD . /home/works/program/
RUN /home/works/python3/bin/pip3.6 install -r /home/works/program/requirements.txt -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com

RUN mkdir -p /home/works/supervisor/logs

EXPOSE 9981

RUN chown works.works -R /home/works/
RUN yum clean all

CMD ["supervisord", "-c", "/etc/supervisord.conf"]