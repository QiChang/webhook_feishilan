# -*- coding: UTF-8 -*-
import os
import sys
import tornado.web
import tornado.ioloop
import tornado.httpserver
from common.wulai_constant import *
from common import mock_user

def load_handlers(name: str) -> list:

    """
    Load the (URL pattern, handler) tuples for each component.

    :param name: handlers dir
    :type name: str
    :return: URL pattern, handler
    :rtype: list
    """
    mod = __import__(name, fromlist=['default_handlers'])
    return mod.default_handlers


class Application(tornado.web.Application):
    def __init__(self):
        handlers = []

        handlers.extend(load_handlers('handlers.Send'))
        handlers.extend(load_handlers('handlers.Webhook'))
        handlers.extend(load_handlers('handlers.Bmi'))
        # set the URL that will be redirected from `/`
        handlers.append(
            (r'/?', tornado.web.RedirectHandler, {
                'url': 'static/a/main.html'
            })
        )

        settings = dict(
            static_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "static"),
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            debug=True
        )

        session_settings = dict(
            driver="memory",
            driver_settings=dict(host=self),
            session_lifetime=1800,  # This setting is only useful in version of torndsession 1.1.5.x
            force_persistence=False)

        settings.update(session=session_settings)
        tornado.web.Application.__init__(self, handlers=handlers, **settings)


if __name__ == "__main__":
    application = Application()

    # print(os.path.join(os.path.dirname(__file__), "static"))
    http_server = tornado.httpserver.HTTPServer(application)
    port = DEFAULT_PORT
    if len(sys.argv) == 3:
        port = int(sys.argv[1])
        current_user = sys.argv[2]
        print("Current user :", current_user, 'INFO')

    http_server.listen(port)
    print("Web run at local port:", port, 'INFO')
    tornado.ioloop.IOLoop.instance().start()

# Sept-12-2018  QiChang.Yin      created.