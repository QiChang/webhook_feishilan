# -*- coding: utf-8 -*-
import datetime
import sys

# Todo 体重增长是否正常
user_dict = {
    "Mary": {
        'height': '1.61',
        'weight_start': '51',
        'BMI_start': 19.6,
        'pre_production_time': '2018-11-12',
        'diabetes_flag': True,
        'hypertension_flag': False,
    },
    "Jane": {
        'height': '1.64',
        'weight_start': '60',
        'BMI_start': 22.3,
        'pre_production_time': '2019-4-30',
        'diabetes_flag': False,
        'hypertension_flag': False,
    },
    "Cathy": {
        'height': '1.71',
        'weight_start': '62',
        'BMI_start': 21,
        'pre_production_time': '2019-5-20',
        'diabetes_flag': True,
        'hypertension_flag': True,
    },
    "Ceci": {
        'height': '1.60',
        'weight_start': '59',
        'BMI_start': 23,
        'pre_production_time': '2019-6-28',
        'diabetes_flag': False,
        'hypertension_flag': True,
    }
}


class MockUser(object):
    def __init__(self, name):
        self.init(name)

    def init(self, name):
        user_info = user_dict[name]
        for k, v in user_info.items():
            setattr(self, k, v)
        # self.height = '1.68'
        # self.weight_start = '51'  # 单位kg
        # self.pre_production_time = '2018-11-25'  # 预产期
        # self.BMI_start = float(self.weight_start) / pow(float(self.height), 2)
        self.weight_now = '00'
        self.preg_weeks = 0  # 怀孕周数
        self.incr_range = [0, 0]  # 孕期体重总增长量范围 单位kg
        self.incr_rate = 0
        self.BMI()
        # self.diabetes_flag = user_dict[name]["diabetes_flag"]
        # self.hypertension_flag = user_dict[name]["hypertension_flag"]
        self.disease_flag = self.diabetes_flag or self.hypertension_flag
        print(self.diabetes_flag,self.diabetes_flag,self.diabetes_flag)
        self.current_status = -1

    def BMI(self):
        v = self.BMI_start
        if v < 19.8:
            self.incr_range = [12.5, 18]
        elif 19.8 < v < 26:
            self.incr_range = [11.5, 16]
        elif 26 < v < 29.9:
            self.incr_range = [7, 11.5]
        elif 30 < v:
            self.incr_range = [6, 16]
        return self.incr_range

    def incr_percent(self):
        w = self.preg_weeks
        if w <= 12 or 36 < w < 40:
            self.incr_rate = 1 / 12
        elif 12 < w <= 36:
            self.incr_rate = 10 / 12

    def cal_pred_weight(self):
        w = self.preg_weeks
        print('****', w)
        pred_w_range = [0, 0]
        if w <= 12:
            pred_w_range = [x * self.incr_rate * w / 12 for x in self.incr_range]
            print('pred_w_range_12', pred_w_range)
        elif 12 < w <= 36:
            pred_w_range_12 = [x * 1 / 12 for x in self.incr_range]
            pred_w_range_36 = [x * self.incr_rate * (w - 12) / 24 for x in self.incr_range]
            pred_w_range = [x + y for x, y in zip(pred_w_range_12, pred_w_range_36)]
            print('pred_w_range_36', pred_w_range_12, pred_w_range_36, pred_w_range)
        elif 36 < w <= 40:
            pred_w_range_12 = [x * 1 / 12 for x in self.incr_range]
            pred_w_range_36 = [x * 10 / 12 for x in self.incr_range]
            pred_w_range_40 = [x * self.incr_rate * (w - 36) / 4 for x in self.incr_range]
            pred_w_range = [x + y + z for x, y, z in zip(pred_w_range_12, pred_w_range_36, pred_w_range_40)]
            print('pred_w_range_40', pred_w_range_12, pred_w_range_36, pred_w_range_40, pred_w_range)
        print(pred_w_range[0], pred_w_range[1], int(self.weight_now) - int(self.weight_start))
        delta = int(self.weight_now) - int(self.weight_start)
        if pred_w_range[0] <= delta <= pred_w_range[1]:
            self.current_status = 0
            # return "体重正常"
        elif delta < pred_w_range[0]:
            self.current_status = 1
            # return "体重过轻"
        elif delta > pred_w_range[1]:
            self.current_status = 2
            # return "体重过重"
        return self.current_status

    def validate_weight_increase_proper(self):
        today = datetime.datetime.now()
        pre_preg_date = datetime.datetime.strptime(self.pre_production_time, '%Y-%m-%d')
        self.preg_weeks = 1 + (280 - (pre_preg_date - today).days) // 7
        print('self.preg_weeks', self.preg_weeks, pre_preg_date - today)
        self.incr_percent()
        result = self.cal_pred_weight()
        return result

    def update_user(self, key, value):
        setattr(self, key, value)


mock_user = MockUser(sys.argv[2])

if __name__ == "__main__":
    print(mock_user.BMI())
    mock_user.update_user('weight_now', '70')
    print(mock_user.validate_weight_increase_proper())
    print(mock_user.disease_flag, mock_user.preg_weeks)
    print('---------')

    mock_user.update_user('weight_now', '60')
    print(mock_user.validate_weight_increase_proper())
    print(mock_user.disease_flag, mock_user.preg_weeks)
    print('---------')

    mock_user.update_user('weight_now', '66')
    print(mock_user.validate_weight_increase_proper())
    print(mock_user.disease_flag, mock_user.preg_weeks)
    print('---------')
