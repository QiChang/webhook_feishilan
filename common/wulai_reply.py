# -*- coding: UTF-8 -*-
from common import wulai_logger as logger
from common import utils
from conf import constant
from .mock_user import mock_user, user_dict
from conf.constant import robot_mes,TP_FLAG_Y,TP_FLAG_N
import re
import sys
def get_reply(params):
    """
       处理 webhook 候选回复， 返回给吾来平台
       :param params: 吾来平台消息路由传入参数
       """
    logger.info("\r\n")
    logger.info("get_reply start".center(80, '-'))
    print('params',params)
    # 获取基本信息
    query = params["msg_body"]['text']['content']
    print(query)

    for one_response in params["bot_response"]["suggested_response"]:
        try:
            source = one_response["source"]
        except Exception:
            source = "DEFAULT_ANSWER_SOURCE"
            logger.info("无法获取任何回复，将进行兜底回复")
        if source == "DEFAULT_ANSWER_SOURCE":
            logger.info("无法召回任何机器人和知识点，将进行兜底回复")
        elif source == "TASK_BOT":

            # 获取实体列表引用和action的值
            task = one_response['detail']['task']
            entities = task['entities']
            action = task['action']
            logger.info('动作:\t%s' % action)

            if query.strip() in {"退出", "清空", "结束", 'quit', 'exit'}:
                # 提前结束任务
                entities = utils.kill_task(entities)
                task['state'] = 1
                task['entities'] = entities
                text = '感谢使用， 再见！'
                utils.set_next_reply(one_response, text)
                one_response['handle_routine'] = {
                    "label_detail": {
                        "is_label": False
                    },
                    "route": True,
                    "route_detail": {
                        "key": ""
                    }
                }
                continue

                # 根据对话单元别名进行逻辑处理
            elif action == '':
                pass

            elif action == "fsl_ask_user_current_weight":
                current_steps = utils.get_entity(entities, 'fsl_prod_current_weight')
                utils.update_entity(one_response, current_steps, TP_FLAG_Y)

            elif action == "fsl_ask_user_everyday_steps":
                fsl_user_input_weight = utils.get_entity(entities, 'fsl_user_input_weight')
                current_weight_value = re.sub("\D", "", str(query))
                if fsl_user_input_weight is not None:
                    if fsl_user_input_weight['value'][0:-6].isdigit:
                        current_weight_value = fsl_user_input_weight['value'][0:-6]
                        mock_user.current_status = mock_user.update_user('weight_now', str(current_weight_value))
                        mock_user.validate_weight_increase_proper()
                        current_steps = utils.get_entity(entities, 'fsl_prod_everyday_steps')
                        utils.update_entity(one_response, current_steps, TP_FLAG_Y)

                elif current_weight_value.isdigit:
                    mock_user.current_status = mock_user.update_user('weight_now', str(current_weight_value))
                    mock_user.validate_weight_increase_proper()
                    current_steps = utils.get_entity(entities, 'fsl_prod_everyday_steps')
                    utils.update_entity(one_response, current_steps, TP_FLAG_Y)
                else:
                    print("data has error")

            elif action == "fsl_has_disease_or_not_continue":
                current_steps = utils.get_entity(entities, 'fsl_prod_uncertain_disease_flag')
                mock_user.disease_flag = user_dict[sys.argv[2]]["diabetes_flag"] or user_dict[sys.argv[2]]["hypertension_flag"]
                if mock_user.disease_flag:
                    if user_dict[sys.argv[2]]["diabetes_flag"] and user_dict[sys.argv[2]]["hypertension_flag"]:
                        str_l = '根据您预留的信息，我们发现您有过{0}，请预约专业的妇产科医生进行咨询。\n'.format("妊娠糖尿病史和妊娠高血压病史")
                        robot_mes['disease_continue'][1]["show_content"] = str_l
                        robot_mes['disease_continue'][1]["msg_body"]["text"]["content"] = str_l
                    elif user_dict[sys.argv[2]]["diabetes_flag"]:
                        str_l = '根据您预留的信息，我们发现您有过{0}，请预约专业的妇产科医生进行咨询。\n'.format("妊娠糖尿病史")
                        robot_mes['disease_continue'][1]["show_content"] = str_l
                        robot_mes['disease_continue'][1]["msg_body"]["text"]["content"] = str_l
                    else :
                        str_l = '根据您预留的信息，我们发现您有过{0}，请预约专业的妇产科医生进行咨询。\n'.format("妊娠高血压病史")
                        robot_mes['disease_continue'][1]["show_content"] = str_l
                        robot_mes['disease_continue'][1]["msg_body"]["text"]["content"] = str_l
                    utils.update_entity(one_response, current_steps, TP_FLAG_Y)
                    one_response["response"] = robot_mes['disease_continue']
                else:
                    utils.update_entity(one_response, current_steps, TP_FLAG_N)
                    if mock_user.current_status == 2:
                        one_response["response"] = utils.set_list_index([
                                                     robot_mes['picture'],
                                                     robot_mes['heavy_info'],
                                                     robot_mes['openpage_questionnaire'],
                                                     robot_mes['openpage_link'],
                                                     robot_mes['understand_more']
                                                     ])
                    elif mock_user.current_status == 1:
                        one_response["response"] = utils.set_list_index([robot_mes['picture'],
                                                    robot_mes['light_info'],
                                                    robot_mes['openpage_questionnaire'],
                                                    robot_mes['openpage_link'],
                                                    robot_mes['understand_more']
                                                    ])
                    else:
                        one_response["response"] = utils.set_list_index(
                                                    [robot_mes['picture'],
                                                    robot_mes['normal_info'],
                                                    robot_mes['understand_more']
                                                    ])

            elif action == "fsl_has_disease_continue_or_not":
                if query in constant.user_continue_reply:
                    current_steps = utils.get_entity(entities, 'fsl_prod_has_disease_flag')
                    utils.update_entity(one_response, current_steps, TP_FLAG_Y)
                    if mock_user.current_status == 2:
                        one_response["response"] = utils.set_list_index([
                                                                         robot_mes['thank_trust'],
                                                                         robot_mes['heavy_info'],
                                                                         robot_mes['openpage_questionnaire'],
                                                                         robot_mes['openpage_link'],
                                                                         robot_mes['understand_more']
                                                                       ])
                    elif mock_user.current_status == 1:
                        one_response["response"] = utils.set_list_index([
                                                                         robot_mes['thank_trust'],
                                                                         robot_mes['light_info'],
                                                                         robot_mes['openpage_questionnaire'],
                                                                         robot_mes['openpage_link'],
                                                                         robot_mes['understand_more']
                                                                        ])
                    else:
                        one_response["response"] = utils.set_list_index([
                                                                         robot_mes['thank_trust'],
                                                                         robot_mes['normal_info'],
                                                                         robot_mes['understand_more']
                                                                        ])
                else:
                    current_steps = utils.get_entity(entities, 'fsl_prod_has_disease_flag')
                    utils.update_entity(one_response, current_steps, TP_FLAG_N)
                    one_response["response"] = utils.set_list_index([robot_mes['end_task']])

                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }
                    }
                    entities = utils.kill_task(entities)
                    task['state'] = 1
                    task['entities'] = entities

            elif action == "fsl_has_not_disease_continue_or_not":
                if query in constant.user_continue_reply :
                    current_steps = utils.get_entity(entities, 'fsl_prod_no_disease_flag')
                    utils.update_entity(one_response, current_steps, TP_FLAG_Y)
                    if mock_user.current_status == 2:
                        one_response["response"] = utils.set_list_index([robot_mes['heavy_advice'],
                                                    robot_mes['end_task']
                                                    ])
                    if mock_user.current_status == 1:
                        one_response["response"] = utils.set_list_index([robot_mes['light_advice'],
                                                    robot_mes['end_task']
                                                    ])
                    if mock_user.current_status == 0:
                        one_response["response"] = utils.set_list_index([robot_mes['normal_advice'],
                                                    robot_mes['end_task']
                                                    ])

                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }
                    }
                    entities = utils.kill_task(entities)
                    task['state'] = 1
                    task['entities'] = entities
                else:
                    current_steps = utils.get_entity(entities, 'fsl_prod_no_disease_flag')
                    utils.update_entity(one_response, current_steps, TP_FLAG_N)
                    one_response["response"] = utils.set_list_index([robot_mes['end_task']])
                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }
                    }
                    entities = utils.kill_task(entities)
                    task['state'] = 1
                    task['entities'] = entities

            elif action == "fsl_has_disease_user_select_continue":
                if query in constant.user_continue_reply:
                    if mock_user.current_status == 2:
                        one_response["response"] = utils.set_list_index([robot_mes['heavy_advice'],
                                                                         robot_mes['end_task']
                                                                         ])
                    if mock_user.current_status == 1:
                        one_response["response"] = utils.set_list_index([robot_mes['light_advice'],
                                                                         robot_mes['end_task']
                                                                         ])
                    if mock_user.current_status == 0:
                        one_response["response"] = utils.set_list_index([robot_mes['normal_advice'],
                                                                         robot_mes['end_task']
                                                                         ])
                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }
                    }
                    entities = utils.kill_task(entities)
                    task['state'] = 1
                    task['entities'] = entities
                else:
                    one_response["response"] = utils.set_list_index([robot_mes['end_task']])
                    one_response['handle_routine'] = {
                        "label_detail": {
                            "is_label": False
                        },
                        "route": True,
                        "route_detail": {
                            "key": ""
                        }
                    }
                    entities = utils.kill_task(entities)
                    task['state'] = 1
                    task['entities'] = entities

            elif action == "fsl_end_message":
                print("fsl_has_disease_user_select_continuefsl_has_disease_user_select_continuefsl_has_disease_user_select_continue")
                one_response["response"] = utils.set_list_index([robot_mes['end_task']])
                one_response['handle_routine'] = {
                    "label_detail": {
                        "is_label": False
                    },
                    "route": True,
                    "route_detail": {
                        "key": ""
                    }
                }
                entities = utils.kill_task(entities)
                task['state'] = 1
                task['entities'] = entities

        else:
            # 召回问答机器人或者关键词机器人
            logger.info("召回问答机器人或者关键词机器人，将不做处理")

    logger.info('[get_reply]:\t文本处理结束')
    return params["bot_response"]["suggested_response"]
